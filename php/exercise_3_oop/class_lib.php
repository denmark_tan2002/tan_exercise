<?php
class PersonalInformation {
	function DataValidation($firstname, $lastname, $age, $submit) {
		if (isset($submit)) { 
			$if_error = false;
			if (empty(trim($firstname))) {
				echo "Input firstname <br/>";
				$if_error = true;
			}
			elseif (!preg_match("/^[a-zA-Z ]+$/",$firstname)) { // + more than 1 and * is more than 0 //validate email
				echo "Only letters and white space allowed <br/>";
				$if_error = true;
			} 
			if (empty(trim($lastname))) {
				echo "Input Lastname <br/>";
				$if_error = true;
			}
			elseif (!preg_match("/^[a-zA-Z ]+$/",$lastname)) {
				echo "Only letters and white space allowed <br/>";
				$if_error = true;
			} 
			if (empty(trim($age))) {
				echo "Input Age";
				$if_error = true;
			}
			elseif (!preg_match("/^[0-9]+$/",$age)) {
				echo "Only numbers<br/>";
				$if_error = true;
			} 
			if($if_error == true) {
			  die();
			} // if
		} // if
    } // function

    function displaytext($firstname, $lastname, $age) {
		echo "<h2>User Input:</h2>";
	    echo "<b>".$firstname.", </b>";
	    echo "<b>".$lastname."</b>";
	    echo "<br>";
		echo "<i>".$age."</i>";
		echo "<br>";
    } // function

    function evenodd ($age) {		    
        if ($age % 2) {
			echo "Odd";
		} else {
			echo"Even";
		}
	        echo"<br/><br/>";
	} // function
} // class
?>
