<!DOCTYPE html>
<html lang="en-US"><!--Languange and Dialects-->
<head>
<title>User Information</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> <!--character encoding-->
<style>
.error {color: #FF0000;}
</style>
</head>
<body style="background-color:lightgrey">

<?php
require('connection/connection.php');
// @$name = $_POST['name'];
// @$age = $_POST['age'];
// @$address = $_POST['address'];
// @$gender = $_POST['gender'];
// @$contact_number = $_POST['contact_number'];
// @$email = $_POST['email'];
// @$submit = $_POST["submit"];
$nameError = $ageError = $addressError = $genderError = $contact_numberError = $emailError = "";

if (isset($_POST["submit"])) {
   if (empty(trim($_POST["name"]))) {
     $nameError = "Name is required.";
   }
   elseif (is_numeric($_POST["name"])) {
     $nameError = "Only letters allowed.";
   } // elseif 
   if (empty(trim($_POST["age"]))) {
     $ageError = "Age is required.";
   } 
   elseif (!is_numeric($_POST["age"])) {
     $ageError = "Only numbers allowed.";
   } // elseif 
   if (empty(trim($_POST["address"]))) {
     $addressError = "Address is required";
   } 
   if ($_POST["gender"] != 'Male' && $_POST["gender"] != 'Female') {
     $genderError = "Gender is required";
   } 
   if (empty(trim($_POST["contact_number"]))) {
     $contact_numberError = "Contact Number is required";
   } 
   else if (!is_numeric($_POST["contact_number"])) {
     $contact_numberError = "Only numbers allowed";
   } // elseif 
   if (empty($_POST["email"])) {
       $emailError = "Email is required";
   } 
   elseif (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
        $emailError = "Invalid email format"; 
    } //elseif
 else {
   $sql = "INSERT INTO `userinformation` (`name`, `age`, `address`, `gender`, `contact_number`, `email`)
      VALUES ('".$_POST["name"]."', '".$_POST["age"]."', '".$_POST["address"]."', '".$_POST["gender"]."', '".$_POST["contact_number"]."', '".$_POST["email"]."')";
      if (mysql_query($sql)) {
          echo "<br/>New record created successfully";
           echo "<form action = AddUserInfo.php>";
           echo "<input type = submit value = Show>";
           echo "</form>";
      } else {
          echo "<br/>Error";
      } // else
 } //else

} //if

?>
<fieldset>
<legend>User Information</legend>
<p><span class="error">*Required field.</span></p>
<form method="post" action="index.php">
<table>
<tr>
<th>Name: </th> <td><input type="text" name="name">
<span class="error">* <?php echo $nameError;?></span>
</td>
</tr>
<tr>
<th>Age: </th> <td><input type="text" name="age">
<span class="error">* <?php echo $ageError;?></span>
</td>
</tr>
<tr>
<th>Address: </th> <td><input type="text" name="address">
<span class="error">* <?php echo $addressError;?></span>
</td>
</tr>
<tr>
<th>Gender: </th> 
<td>
<input type="radio" name="gender" value="Male" checked="checked">Male
<input type="radio" name="gender" value="Female">Female</td>
<span class="error">* <?php echo $genderError;?></span>
</td>
</tr>
<tr>
<th>Contact Number: </th> <td><input type="text" name="contact_number">
<span class="error">* <?php echo $contact_numberError;?></span>
</td>
</tr>
<tr>
<th>Email: </th> <td><input type="text" name="email">
<span class="error">* <?php echo $emailError;?></span>
</td>
</tr>
<tr>
<td><input type="submit" value ="Submit" name="submit">
<input type="reset" name="Reset" id="button" value="Reset">
</td>
</tr>
</table>
</form>
</fieldset>
</body>
</html>

