<?php
class GCD {
	function DataValidation($firstnumber, $secondnumber) {
		    $if_error = false;
		if (!is_numeric(trim($firstnumber))) {
			echo "Input Integer<br/>";
			$if_error = true;
		}
		elseif (!preg_match("/^[0-9]*$/",$firstnumber)) {
			echo "Only numbers allowed<br/>";
			$if_error = true;
		} 
		if (!is_numeric(trim($secondnumber))) {
			echo "Input Integer<br/>";
			$if_error = true;
		}
		elseif (!preg_match("/^[0-9]*$/",$secondnumber)) {
			echo "Only numbers allowed<br/>";
			$if_error = true;
		}
		if($if_error == true) { //if_error is true it will exit.
		  	die();
		} // if
	} // function
	function getGCD($firstnumber, $secondnumber) {// function for getting gcd	
		if ($firstnumber == 0 || $secondnumber == 0) {// if condition
	        return abs( max(abs($firstnumber), abs($secondnumber)) ); // using absolute value and highest value
	    } //if    
	    while($firstnumber > 0) { //while condition
	        $z = $firstnumber; //store value of variable a to variable z
	        $firstnumber = $secondnumber % $firstnumber; // use modulo to calculate the remainder of a division
	        $b = $z;  //store value of variable z to variable b
		}
		return $z; // echo z
    } // function
} // class
?>