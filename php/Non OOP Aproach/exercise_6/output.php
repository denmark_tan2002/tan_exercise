<?php
    $number = $_POST["number"]; //declaration of variable
    function fizzbuzz($number) { //function fizzbuzz with arguments
    	if (!(is_numeric($number))) {
    		return "Input Integer <br/>";
    		die ();
    	}
    	else { 
    	    for ($index=1; $index<=$number; $index++) { //for statement
    	        if ($index%15==0) { //if condition for fizz buzz
     	            echo $index." FizzBuzz <br/>";
                }
                elseif ($index%5==0) { //else if condition for buzz
     	            echo $index." Buzz <br/>";	
                }	
                elseif ($index%3==0) { //if condition for fizz
     	            echo $index." Fizz <br/>";
                }  	
                else { //if the conditions are not satisfied the index will output
    	            echo $index."<br/>";
                } // else
            }//for
        	
    	} //else
    } //function
	echo fizzbuzz($number); //call the function with arguments
?>