<?php
$fullname = $_POST["fullname"];
$age = $_POST["age"];
$emailaddress =$_POST["emailaddress"];
$submit = $_POST["submit"];
function validationfunction($fullname,$age,$emailaddress,$submit) { // function 
    if (isset($submit)) {
        $if_error = false;
	 		  if (empty(trim($fullname))) {
     		    echo "Input Fullname <br/>";
            $if_error = true;
   			} // if 
   			elseif (!preg_match("/^[a-zA-Z ]+$/",$fullname)) {
       			echo "Only letters and white space allowed <br/>";
            $if_error = true;
     		} // elseif
     		if (empty(trim($age))) {
				    echo "Input Age <br/>";
            $if_error = true;
        } // if 
			  elseif (!preg_match("/^[0-9]+$/",$age)) {
				    echo "Only numbers allowed<br/>";
            $if_error = true;
			  } // elseif
			  if (empty(trim($emailaddress))) {
     		    echo "Input Email<br/>";
            $if_error = true;
   			} //if 
   			elseif (!filter_var($emailaddress, FILTER_VALIDATE_EMAIL)) {
       	    echo "Invalid email format<br/>"; 
            $if_error = true;
     		} //elseif
        if($if_error == true) {
            die();
        }//if
   		} // if
   		echo "<h2>User Input:</h2>";
      echo ("<b>".$fullname."</b><br/>");
      echo ("<b>".$age."</b><br/>");
      echo ("<i>".$emailaddress."</i><br/>"); 
      $cvsData = $fullname . "," . $age . "," . $emailaddress ;
      $filepointer = fopen("userinformation.csv","a"); // open to file userinformation.csv
      fwrite($filepointer,$cvsData."\r\n"); //write information to the file 
      fclose($filepointer); //close    
	} // function
	echo validationfunction($fullname,$age,$emailaddress,$submit); // calling validation function
?>