<!DOCTYPE html>
<html lang="en-US"><!--Languange and Dialects-->
<head>
<title>Personal Information List</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"> <!--character encoding-->
</head>
<body style="background-color:lightgrey">
<form action="index.php" method="post" enctype="multipart/form-data">
<input type="submit" value ="Add User" name="adduser">
</form>
<legend>Personal Information List</legend><br/>
</body>
</html>
<?php
$fullname = $_POST["fullname"];
$age = $_POST["age"];
$emailaddress = $_POST["emailaddress"];
$submit = $_POST["submit"];
$name = $_FILES["file"]["name"];
$size = $_FILES["file"]["size"];
$tmp_name = $_FILES["file"]["tmp_name"];
if (isset($name)) {
  if (!empty($name)) {
      $location = "upload/";
    if (move_uploaded_file($tmp_name, $location.$name)) {
        echo "Uploaded<br/> Successfully Added!";
    } //if
  else {
    echo "Please upload a picture";
  } //else
 }//if
}//if 
function validationfunction($fullname,$age,$emailaddress,$submit) { // function 
  if (isset($submit)) {
    $if_error = false;
    if (empty(trim($fullname))) {
     echo "<br/>Input Fullname <br/>";
     $if_error = true;
   			} // if 
   			elseif (!preg_match("/^[a-zA-Z ]+$/",$fullname)) {
          echo "<br/>Only letters and white space allowed <br/>";
          $if_error = true;
     		} // elseif
     		if (empty(trim($age))) {
          echo "<br/>Input Age <br/>";
          $if_error = true;
        } // if 
        elseif (!preg_match("/^[0-9]+$/",$age)) {
          echo "<br/>Only numbers allowed<br/>";
          $if_error = true;
			  } // elseif
			  if (empty(trim($emailaddress))) {
         echo "<br/>Input Email<br/>";
         $if_error = true;
   			} //if 
   			elseif (!filter_var($emailaddress, FILTER_VALIDATE_EMAIL)) {
          echo "<br/>Invalid email format<br/>"; 
          $if_error = true;
     		} //elseif
        if($if_error == true) {
          die();
        }//if
   		} // if
} // function
echo validationfunction($fullname,$age,$emailaddress,$submit); // calling validation function
$cvsData = ucfirst($fullname).",".$age.",".$emailaddress.",".$name;
$filepointer = fopen("userinformation.csv","a"); // open to file userinformation.csv
fwrite($filepointer,$cvsData."\r\n"); //write information to the file 
fclose($filepointer); //close
?>