<html>
<head>
</head>
<body>
<?php
	$a = $_POST["firstnumber"]; //declarations of variable
	$b = $_POST["secondnumber"];
	$if_error = false;
	if (empty(trim($_POST["firstnumber"]))) {
		echo "Input Integer<br/>";
		$if_error = true;
	}
	elseif (!preg_match("/^[0-9]*$/",$_POST['firstnumber'])) {
		echo "Only numbers allowed<br/>";
		$if_error = true;
	} 
	if (empty(trim($_POST["secondnumber"]))) {
		echo "Input Integer<br/>";
		$if_error = true;
	}
	elseif (!preg_match("/^[0-9]*$/",$_POST['secondnumber'])) {
		echo "Only numbers allowed<br/>";
		$if_error = true;
	}
	if($if_error == true) { //if_error is true it will exit.
	  	die();
	}
Function getGCD($a, $b) // function for getting gcd
{	global $a, $b; // global declaration
	if ($a == 0 || $b == 0) {// if condition
        return abs( max(abs($a), abs($b)) ); // using absolute value and highest value
    }    
    while($a > 0) { //while condition
      $z = $a; //store value of variable a to variable z
      $a = $b % $a; // use modulo to calculate the remainder of a division
      $b = $z;  //store value of variable z to variable b
	}
	return $z; // echo z
}
echo getGCD($a, $b); // calling the function
?>
</body>
</html>
